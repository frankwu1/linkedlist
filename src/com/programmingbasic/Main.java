package com.programmingbasic;

public class Main {

    public static void main(String[] args) {
	// write your code here

        LinkedList<Object> list = new LinkedList<>();
        list.add(10);
        list.add("Hello");
        list.add("Hi");
        TestObject s = new TestObject(10,"linked list");
        list.add(s);
        list.dump();
        System.out.println(list.size());

        list.add(2,100);
        list.dump();
        System.out.println(list.size());

        list.remove(3);
        list.dump();
        System.out.println(list.size());

        list.remove(0);
        list.dump();
        System.out.println(list.size());

        list.remove(0);
        list.dump();
        System.out.println(list.size());

        list.remove(0);
        list.dump();
        System.out.println(list.size());

        list.remove(0);
        list.dump();
        System.out.println(list.size());

    }

}

