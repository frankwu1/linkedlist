package com.programmingbasic;

public class TestObject {
    private int value;
    private String text;
    public TestObject(int v, String t){
        this.value = v;
        this.text = t;
    }

    @Override
    public String toString() {
        return "TestObject{" +
                "value=" + value +
                ", text='" + text + '\'' +
                '}';
    }
}