package com.programmingbasic;

public class LinkedList<E>{
    private Node<E> head;
    private Node<E> last;
    private int size;

    public LinkedList(){
        head = null;
        last = null;
        size = 0;
    }

    public void add(E elem){
        Node n = new Node(elem);
        if ( size == 0 ) {
            head = n;
            last = head;
        } else {
            last.setNext(n);
            last = last.getNext();
        }
        size++;
    }

    public void add(int index,E elem){
        Node n = new Node(elem);
        Node next = getNode(index);

        if ( next.equals(head) ) {
            n.setNext(head.getNext());
            head = n;
            size++;
            return ;
        }

        Node prev = getNode(index-1);
        prev.setNext(n);
        n.setNext(next);
        size++;
    }

    public Node getNode(int index){

        if ( index < 0 || index >= this.size() ) {
            throw new IndexOutOfBoundsException();
        }

        Node last = head;
        int count = 0;
        while ( count != index ) {
            last = last.getNext();
            count++;
        }
        return last;
    }

    public void remove(int index){
        if ( getNode(index).equals(head) ) {
            head = head.getNext();
        } else {
            Node prev = getNode(index - 1);
            Node del = getNode(index);
            prev.setNext(del.getNext());
        }
        size--;
    }


    public int size(){
        return size;
    }

    public boolean isEmpty(){
        if ( size == 0 ) {
            return true;
        }
        return false;
    }

    public void dump(){

        if ( isEmpty() ) {
            System.out.println("list is empty");
            return ;
        }

        Node n = head;
        System.out.print("HEAD-" + n.getElem());
        while ( n.hasNext() ) {
            n = n.getNext();
            System.out.print(" > " + n.getElem());
        }
        System.out.println();

    }

}
