package com.programmingbasic;

public class Node<E> {
    private E elem;
    private Node next = null;
    private Node prev = null;

    public Node(E elem){
        this.elem = elem;
    }

    public E getElem() {
        return elem;
    }

    public void setElem(E elem) {
        this.elem = elem;
    }

    public boolean hasNext(){
        if ( this.next != null ) {
            return true;
        }
        return false;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext(){
        return this.next;
    }

    @Override
    public String toString() {
        return "Node{" +
                "elem=" + elem +
                ", Next=" + next +
                ", Prev=" + prev +
                '}';
    }
}
